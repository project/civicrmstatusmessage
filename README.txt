INTRODUCTION
------------
Display CiviCRM status messages on non-CiviCRM Drupal pages for authenticated
users.


EXAMPLE USE CASE
----------------
If you have a CiviCRM Profile that redirects to a Drupal page on submit, the
success message normally wouldn't appear until the next time the user lands on a
CiviCRM page. This module extracts all messages from CiviCRM's session, and
displays them on Drupal pages as well.


REQUIREMENTS
------------
This module requires the following modules:
 * CiviCRM (https://civicrm.org/)

 
INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------
No configuration is required for this module
